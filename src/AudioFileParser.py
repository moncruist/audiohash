'''
Created on 19.09.2012

@author: kzhukov
'''

import wave
import numpy as np
import math

types = {
    1: np.int8,
    2: np.int16,
    4: np.int32
}


class AudioFileParser:
    '''
    classdocs
    '''
    WINDOW_MS = 128  # Длина фрейма в миллисекундах

    def __init__(self, filename):
        '''
        Constructor
        '''
        self.filename = filename
        self.nchannels = None
        self.sample_width = None
        self.frame_rate = None
        self.nframes = None
        self.frames_in_window = None
        self.windows = []

    def file_info(self):
        '''
        Prints audio file info
        '''
        wav_file = wave.open(self.filename, 'r')
        print("""Filename: {0}
        Channels: {1}
        Framerate: {2}
        Frames: {3}
        Samplewidth: {4}""".format(self.filename, wav_file.getnchannels(), \
                                   wav_file.getframerate(), wav_file.getnframes(), \
                                   wav_file.getsampwidth()))
        wav_file.close()
        
    def __read_all_frames(self):
        wav_file = wave.open(self.filename, 'r')
        (self.nchannels, self.sample_width, self.frame_rate, self.nframes, comptype, compname) = wav_file.getparams()
        audio_frames_data = wav_file.readframes(self.nframes)
        wav_file.close()
        self.content = np.fromstring(audio_frames_data, dtype=types[self.sample_width])
        
    def __normalize(self):
        '''
        Sound normalization
        '''
        max = np.max(self.content)
        self.content = self.content / max
    
    def __generate_windows(self, frames_in_window):
        '''
        Split array into "windows". All rest work will be only with windows.
        Windows is half-period overlapped.
        '''
        half_window_frames = frames_in_window / 2
        idx = 0
        length = len(self.content)
        splices = []
        while idx < length:
            if (idx + frames_in_window) < length:
                splices.append(self.content[idx:idx+frames_in_window])
                idx += half_window_frames
            else:
                splices.append(self.content[idx:])
                idx = length
        for window in splices:
            self.windows.append(window.copy())

    def parse(self):
        self.__read_all_frames()
        self.__normalize()
        self.frames_in_window = self.frame_rate * self.WINDOW_MS / 1000
        self.__generate_windows(self.frames_in_window)
        
        
def format_time(x, pos=None):
    progress = x / 6144 * 128
    return "{0:.4f}".format(progress)

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    parser = AudioFileParser('../samples/sample1.wav')
    parser.parse()
    plt.figure()
    print(len(parser.windows[10]))
    data = parser.windows[10].copy()
    #for i in range(10):
    #    data = np.append(data, parser.windows[i+1])
    axes = plt.plot(data)
    plt.ylim(-1.0,1.0)
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(format_time))
    plt.ylabel("Amplitude")
    plt.xlabel("Time, ms")
    plt.show()
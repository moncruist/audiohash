'''
Created on 29.09.2012

@author: Moncruist
'''
from AudioFileParser import AudioFileParser
import numpy as np
import math
from scipy.fftpack import dct

class AudioHash():
    '''
    classdocs
    '''


    def __init__(self, filename):
        '''
        Constructor
        '''
        self.parser = AudioFileParser(filename)
        self.hwindowed = []
        self.ffts = []
        self.maxHz = 20000
        self.minHz = 0
        self.numCoefficients = 20
        self.filtered = []
        self.mfcc = []
    
    def hash(self):
        self.parser.parse()
        windows = self.parser.windows
        for window in windows:
            hamming = np.hamming(len(window))
            window_with_hamming = np.multiply(window, hamming)
            self.hwindowed.append(window_with_hamming)
            window_with_fft = np.fft.fft(window_with_hamming)
            window_with_fft = abs(window_with_fft) ** 2
            self.ffts.append(window_with_fft)
            filteredLogSpectrum = np.log(np.dot(window_with_fft, self.melFilterBank(len(window_with_fft))))
            self.filtered.append(filteredLogSpectrum)
            dctSpectrum = self.compute_mfcc(filteredLogSpectrum)
            self.mfcc.append(dctSpectrum)
            
    def hertz_to_mel(self, value):
        mel = 2595 * math.log10(1 + value / 700)
        return mel
    
    def mel_to_hertz(self, value):
        hertz = 700 * (math.pow(10, value / 2595) - 1)
        return hertz
    
    def mel_to_hertz_np(self, array):
        hertz_array = 700 * (np.power(10, array / 2595) - 1)
        return hertz_array
    
    def melFilterBank(self, blockSize):
        numBands = int(self.numCoefficients)
        maxMel = int(self.hertz_to_mel(self.maxHz))
        minMel = int(self.hertz_to_mel(self.minHz))
    
        # Create a matrix for triangular filters, one row per filter
        filterMatrix = np.zeros((numBands, blockSize))
    
        melRange = np.array(range(numBands + 2))
    
        melCenterFilters = melRange * (maxMel - minMel) / (numBands + 1) + minMel
        hertzCenterFilters = self.mel_to_hertz_np(melCenterFilters)
        
        for i in range(self.numCoefficients):
            start, centre, end = hertzCenterFilters[i:i+3]
            start = int(start * blockSize / self.parser.frame_rate)
            centre = int(centre * blockSize / self.parser.frame_rate)
            end = int(end * blockSize / self.parser.frame_rate)
            k1 = np.float32(centre - start)
            k2 = np.float32(end - centre)
            up = (np.array(range(start,centre)) - start) / k1
            down = (end - np.array(range(centre,end))) / k2
            
            filterMatrix[i][start:centre] = up
            filterMatrix[i][centre:end] = down
            
        return filterMatrix.transpose()

    def compute_mfcc(self, filteredLogSpectrum):
        coefs = []
        for i in range(self.numCoefficients):
            sum = 0.0
            for j in range(len(filteredLogSpectrum)):
                spectre = filteredLogSpectrum[j]
                sum += spectre * math.cos(math.pi * (i + 1) * (j + 0.5) / self.numCoefficients)
            coefs.append(sum)
        return coefs
        

def format_time(x, pos=None):
    progress = x / 6144 * 128
    return "{0:.4f}".format(progress) 
    
def format_frequency(x, pos=None):
    freq = int(48000 / 6144 * x)
    return "{0}".format(freq)

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    hash = AudioHash('../samples/sample1.wav')
    hash.hash()
    
    #print(int(hash.mel_to_hertz(1000)))
    plt.figure()
    data = hash.mfcc[10]
    #matrix = hash.melFilterBank(len(data))
    #data = hash.hwindowed[10].copy()
    #for i in range(10):
    #    data = np.append(data, hash.hwindowed[i+1])
    plt.plot(data, 'ro')
    #print(matrix[0])
    print(data)
    #plt.ylim(-1.0,1.0)
    #plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(format_frequency))
    #plt.ylabel("Amplitude")
    #plt.xlabel("Frequency, Hz")
    plt.show()
